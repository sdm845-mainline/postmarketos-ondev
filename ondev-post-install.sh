#!/bin/sh -ex
# Copyright 2021 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later

# Shell commands running after the installation is complete. This gets called
# by shellprocess.conf, as written by ondev-prepare.sh.

if [ -e /tmp/ondev-internal-storage ]; then
	# External to internal storage
	
	# just poweroff, so the user can pull out the SD card before next boot.
	# It would be nice if we could show a "finished" screen instead of
	# doing this right after the install is done without further notice.
	# See: https://github.com/calamares/calamares/issues/1601
	poweroff
else
	# Installed to same storage as the installation medium (e.g. SD to SD,
	# or eMMC to eMMC in factory image)

	# Rename install partition, so the initramfs will boot into the new
	# pmOS_root partition instead (unless installed from external to
	# internal storage)
	tune2fs -L pmOS_deleteme /dev/disk/by-label/pmOS_install

	# Rename the installer's boot partition, in case the on-device
	# installer image was flashed to eMMC and installed there (like in
	# the factory). If the user plugs an SD card with the on-device
	# installer afterwards, we want that installer to prefer its own
	# pmOS_inst_boot partition over the pmOS_boot partition on the
	# eMMC. ("pmOS_i_boot" fits 11 chars for fat32; "pmOS_inst_boot" is for
	# backwards compatibility with older pmbootstrap versions)
	for part in pmOS_i_boot pmOS_inst_boot; do
		part_path=/dev/disk/by-label/"$part"
		if [ -e "$part_path" ]; then
			tune2fs -L pmOS_boot "$part_path"
		fi
	done

	reboot
fi
